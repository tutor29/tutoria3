const { Router } = require('express');
const Category = require('../models/Category');

const router = Router();

router.get('/:name', (req, res) => {
    //res.send(`<h1>${req.params.name}</h1>`)
})

router.post('/', async (req, res) => {
    try {
        const newCategory = new Category({
            ...req.body,
        });
        const savedCategory = await newCategory.save();
        res.json(savedCategory);
    } catch (error) {
        res.send(error);
    }
})

module.exports = router;
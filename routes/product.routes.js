const { Router } = require('express');
const Product = require('../models/Product');

const router = Router();


//ruta para consultar todos los productos 
router.get('/', async (req, res) => {
    try {
        // populeta sirve para rellenar los datos de un atributo del documento padre
        const products = await Product.find().populate('category');
        res.json(products);
    } catch (error) {
        res.send(error);
    }
})
// ruta pra agregar productos
router.post('/', async (req, res) => {
    //const { name, brand, amount, price, description, category } = req.body;
    // const newProduct = new Product({
    //     name,
    //     brand,
    //     amount,
    //     description,
    //     category,
    // })
    try {
        const newProduct = new Product({
            ...req.body,
        });
        const savedProduct = await newProduct.save();
        res.json(savedProduct);
    } catch (error) {
        res.send(error);
    }
});
// ruta para consutar producpor por id
router.get('/:id', async (req, res) => {
    try {
        const product = await Product.findById(req.params.id).populate('category');
        res.json(product);
    } catch (error) {
        res.send(error);
    }
})
// ruta para elimnar productos
router.delete('/:id', async (req, res) => {
    try {
        const product = await Product.findByIdAndRemove(req.params.id).populate('category');
        res.json(product);
    } catch (error) {
        res.send(error);
    }
})
// metodo de actulización de productos
router.put('/:id', async (req, res) => {
    try {
        const updateProduct = await Product.findByIdAndUpdate(req.params.id, {...req.body}, { returnDocument: 'after' });
        console.log(updateProduct);
        const savedProduct = await updateProduct.save();
        res.json(savedProduct);
    } catch (error) {
        res.send(error);
    }
})

module.exports = router;
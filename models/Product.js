const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    brand: String,
    amount: {
        type: Number,
        required: true,
        min: 0,
    },
    price: {
        type: Number,
        required: true,
        min: 0,
    },
    description: {
        type: String,
        validate: {
            validator: function(){
                return this.description.length < 200;
            },
            message: 'length must be less than 200'
        }
    },
    category: {
        type: mongoose.Types.ObjectId,
        ref: 'Category'
    }
},
{
    timestamps: true,
});

module.exports = mongoose.model('Product', productSchema);
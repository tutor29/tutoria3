const mongoose = require('mongoose');

const categorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        validate: {
            validator: function(){
                return this.description.length < 200;
            },
            message: 'length must be less than 200'
        }
    },
});

module.exports = mongoose.model('Category', categorySchema);
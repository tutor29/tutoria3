const express = require('express');
const productRoutes = require('./routes/product.routes');
const categoryRoutes = require('./routes/category.routes');
const app = express();

// Tipo de salida de las respuestas
app.use(express.json());

// RUTAS
app.use('/products', productRoutes);
app.use('/categories', categoryRoutes);

module.exports = app;
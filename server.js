const app = require('./app');
require('./database');

const port = 5000;

// Inicialización del servidor de express
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
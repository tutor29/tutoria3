const mongoose = require('mongoose');

const MONGODB_URI = "mongodb://localhost:27017/tutoria_magistral";

mongoose.connect(MONGODB_URI)
    .then(console.log('Database is connected to tutoria_magistral'))
    .catch( err => console.log(err))